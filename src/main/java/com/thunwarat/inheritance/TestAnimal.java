/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.inheritance;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main (String[] args){
        Animal animal = new Animal("Ani ", "white",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang ", "Black&White");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("To ", "ChaThai");
        to.speak();
        to.walk();
        
        Dog bat = new Dog("Bat ", "OffWhite");
        bat.speak();
        bat.walk();
        
        Dog mome = new Dog("Mome ", "FaceDark");
        mome.speak();
        mome.walk();
        
        Cat zero = new Cat("Zero ", "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom ", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck somgab = new Duck("SomGab ", "Black&Yellow");
        somgab.speak();
        somgab.walk();
        somgab.fly();
        
        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Cat: " + (zom instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is animal: " + (animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: zom is Duck " + (ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero, zom};
        for(int i = 0 ;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                     Duck duck = (Duck)animals[i];
                     duck.fly();
            }
        }
    }
}
